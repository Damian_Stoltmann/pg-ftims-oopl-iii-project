package notepadOJP;

import javax.swing.JTextPane;

public class TextEditor extends JTextPane {
	
	private static final long serialVersionUID = 1L;
	public final static String AUTHOR_EMAIL = "damian.stoltmann94@gmail.com";
	public final static String NAME = "NotePad";
	public final static double VERSION = 1.0;

	
	public static void main(String[] args) {
        new Main().setVisible(true);
	}

}
