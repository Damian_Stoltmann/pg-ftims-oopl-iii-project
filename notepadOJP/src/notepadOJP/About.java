package notepadOJP;

import javax.swing.*;
import java.awt.FlowLayout;

public class About {

    private final JFrame frame;
    private final JPanel panel;
    private String contentText;
    private final JLabel text;
  
	public About() {
		panel = new JPanel(new FlowLayout());
		panel.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 8));
		frame = new JFrame();
		frame.setVisible(true);
		frame.setSize(500,300);
		text = new JLabel();
		
	}
	
	public void me() {
		frame.setTitle("About Me - " + TextEditor.NAME);
		
		contentText = 
		"<html><body><p>" +
		"Author: Damian Stoltmann<br />" +
		"Contact me at: " +
		"<a href='mailto:" + TextEditor.AUTHOR_EMAIL + "?subject=Notepad project'>" + TextEditor.AUTHOR_EMAIL + "</a>" +
        "<br /><br />" +
		
         "</a>" +
		"</p></body></html>";
		
		text.setText(contentText);
		panel.add(text);
		frame.add(panel);
	}
    
	public void software() {
		frame.setTitle("About Me - " + TextEditor.NAME);
		
		contentText = 
		"<html><body><p>" +		
		"Name: " + TextEditor.NAME + "<br />" +
		"Version: " + TextEditor.VERSION + 
		"</p></body></html>";
		
		text.setText(contentText);
		panel.add(text);
		frame.add(panel);
	}
	
}
