# Object-Oriented Programming Languages III final project #


Author: Damian Stoltmann
Mail: damian.stoltmann94@gmail.com

##Project information
My application is a simple text editor ,where u can :
* create new file ,
* open file , 
* save your file , 
* clear text ,
* quick search some words ,
* use keyboard's shortcuts ,
* move your mouse over the icon and find out what do ,
 
**Application uses Swing library.

##How to run / compile
-The project is compiled into a jar file
-run "Notepad.jar" file from notepadOJP
##